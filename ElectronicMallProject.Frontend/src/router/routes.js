import Layout from '../components/Frontpage/Layout.vue'


let routes = [
    {
        name: "Layout",
        title: "前台布局框架",
        path: "/",
        component: Layout,
        children: [
            {
                name: "Home",
                title: "前台首页",
                path: "home",
                component: () => import('../components/Frontpage/Views/Home.vue')
            }
        ]
    },
    {
        title:"登陆",
        component:()=>import("../components/Frontpage/Views/Login"),
        path:"/login"
    },
    {
        title:"注册",
        component:()=>import("../components/Frontpage/Views/Register"),
        path:"/register"
    },
  
    {
        title:"生成订单和支付",
        component:()=>import('../components/Frontpage/Views/GenerateOrderAndPay'),
        path:"/generateorderandpay"
    },
    {
        title:"商品分类",
        component:()=>import("../components/Frontpage/Views/Category"),
        path:"/category"
    },
    {
        title:"商品详情",
        component:()=>import("../components/Frontpage/Views/CommodityDetails"),
        path:"/commoditydetails"
    },
    {
        title:"购物车",
        component:()=>import("../components/Frontpage/Views/ShoppingCart"),
        path:"/shoppingcart"
    },
    {
        title:"订单",
        component:()=>import("../components/Frontpage/Views/Order"),
        path:"/order"
    },
]




export default routes;