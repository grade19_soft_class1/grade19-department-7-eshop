import res from "../utils/index";


//获取商品表
export function getCommodity() {
    return res.get(`/commodity`);
}

//获取订单表
export function getSales() {
    return res.get(`/sales`);
}

//订单表和商品表连接查询
export function getCommodityAndSales(id) {
    return res.get(`/sales/${id}`)
}

//购物车
export function getShoppingCart() {
    return res.get(`/shoppingcart`)
}

//通过用户Id查找该用户购物车记录同时获取商品表,类型表,品牌表
export function getShoppingCartRecordAndCommodityMsgByUserId(id) {
    return res.get(`/shoppingcart/${id}/getuserId`)
}

//获取所有商品类型
export function getShopType() {
    return res.get(`shoptype/getchildren`)
}

//查看商品表和图片
export function getCommodityImg(data) {
    return res.get(`/commodity/CommodityImgGet`, data)
}

//加入购物车的
export function addShoppingCart(data){
    return res.post(`/shoppingcart/Addshoppingcart`,data)
}

//通过商品Id查找其详情图片
export function findDetaildImgsByCommodityId(id){
    return res.get(`/commodity/${id}/getCommodityById`)
}

//修改购物车
export function changeShoppingCart(id,data){
    return res.put(`/shoppingcart/${id}/Updateshoppingcart`,data);
}