import res from "../utils/index";

export function getUsers(){
    return res.get(`/users/getusers`);
}

//修改用户信息
export function changeUserMsg(id,data){
    return res.put(`/users/${id}/changeusercustomerorderaddress`,data)
}