const TokenKey = 'token'
const RefreshTokenKey = 'refreshToken'
const UserIdKey = 'userId'
const UserNameKey = 'userName'
const UserRoleKey = "userRole"
const UserPassword = "userPassword"

//获取token
export function getToken() {
    return localStorage.getItem(TokenKey) || ''
}

//获取refreshToken
export function getrefreshToken() {
    return localStorage.getItem(RefreshTokenKey) || ''
}

//保存用户ID
export function SetUserId(UserId) {
    localStorage.setItem(UserIdKey, UserId)
}

//保存用户名
export function SetUserName(UserName) {
    localStorage.setItem(UserNameKey, UserName)
}

// 保存token和refreshToken
export function setToken(token, refreshToken) {
    localStorage.setItem(TokenKey, token),
        localStorage.setItem(RefreshTokenKey, refreshToken);
}
// 清除token,refreshToken,用户Id，用户名,用户角色,用户密码
export function clearToken(token, refreshToken, UserId, UserName,UserRole,PassWord) {
    localStorage.removeItem(TokenKey, token);
    localStorage.removeItem(RefreshTokenKey, refreshToken);
    localStorage.removeItem(UserIdKey, UserId);
    localStorage.removeItem(UserNameKey, UserName);
    localStorage.removeItem(UserRoleKey, UserRole)
    localStorage.removeItem(UserPassword, PassWord)
}

//保存用户角色
export function SetUserRole(UserRole) {
    localStorage.setItem(UserRoleKey, UserRole)
}

//保存用户密码
export function SetPassWord(PassWord){
    localStorage.setItem(UserPassword,PassWord)
}


//判断是否登录
export function isLogin() {
    let token = getToken();
    if (token) {
        return true
    } else {
        return false
    }
}