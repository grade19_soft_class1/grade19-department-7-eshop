import Vue from 'vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import App from './App.vue'
import router from './router/index.js'
import { VueJsonp } from 'vue-jsonp'
// import VueAMap from "vue-amap";

// Vue.use(VueAMap);

// VueAMap.initAMapApiLoader({
//   key: '7ab53b28352e55dc5754699add0ad862',
//   plugin: ['AMap.Autocomplete', 'AMap.PlaceSearch', 'AMap.Scale', 'AMap.OverView', 'AMap.ToolBar', 'AMap.MapType', 'AMap.PolyEditor', 'AMap.CircleEditor','AMap.Geolocation','Geocoder'],//plugin所要用到的模块功能，按需添加
//   v: '1.4.4',//高德 sdk 版本为 1.4.4
// });


Vue.config.productionTip = false
Vue.use(ElementUI);
Vue.use(VueJsonp);

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
