using ElectronicMallProject.Backend.Api.Entity;
using Microsoft.EntityFrameworkCore;

namespace ElectronicMallProject.Backend.Api.Database
{
    public class DbInit : DbContext
    {
        public DbInit()
        {

        }

        public DbInit(DbContextOptions options) : base(options)
        {

        }

        public DbSet<Users> users { get; set; }
        public DbSet<Account> account { get; set; }
        public DbSet<AddRess> address { get; set; }

        //审计日志
        public DbSet<AuditInfo> auditinfo { get; set; }
        public DbSet<Brand> brand { get; set; }
        public DbSet<Carousels> carousels { get; set; }
        public DbSet<Comment> comment { get; set; }
        public DbSet<Commodity> commodity { get; set; }
        public DbSet<CommodityDisplayColumn> commoditydisplaycolumn { get; set; }
        public DbSet<Imgs> imgs { get; set; }
        public DbSet<Logistics> logistics { get; set; }
        public DbSet<Sales> sales { get; set; }
        public DbSet<Shop> shop { get; set; }
        public DbSet<ShoppingCart> shoppingcart { get; set; }
        public DbSet<ShopType> shoptypes { get; set; }
        public DbSet<Specifications> Specifications { get; set; }
        public DbSet<Supplier> supplier { get; set; }
        public DbSet<UserInfo> userinfo { get; set; }
        public DbSet<DetailsChart> DetailsCharts {get;set;}
        
        


        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseNpgsql(@"server=47.113.121.34;database=EMMS;uid=postgres;pwd=XiongYY200012152841735357***;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // 初始化一个管理员用户 虽然Id是自动生成，但此处必须明确指定
            modelBuilder.Entity<Users>().HasData(
                new Users
                {
                    Id = 1,
                    UserName = "admin",
                    PassWord = "113",
                    PhoneNumber = "13174562456",
                    Address = null,
                    CustomerOrderAddress = null,
                    UserRole = "管理员",
                    IsActived = true,
                    IsDeleted = false,
                    CreatedTime = System.DateTime.Now,
                    UpdatedTime = System.DateTime.Now,
                    DisplayOrder = 0,
                    Remarks = null
                }
            );
            base.OnModelCreating(modelBuilder);
        }

    }
}