using ElectronicMallProject.Backend.Api.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace ElectronicMallProject.Backend.Api.Repository
{
    /// <summary>
    /// 定义的一个接口，用于实体类型的CRUD操作
    /// </summary>
    /// <typeparam name="T">泛型，可以表示任意一个实体</typeparam>
    public interface IRepository<T> where T : BaseInit
    {
        /// <summary>
        /// 一个属性表示可查询的表
        /// </summary>
        /// <value></value>
        IQueryable<T> Table {get;}
        /// <summary>
        /// 获取实体类型T
        /// </summary>
        /// <value></value>
        T GetById(object Id);
        /// <summary>
        /// 用于插入数据
        /// </summary>
        /// <param name="entity">获取的实体类型</param>
        void Insert(T entity);
        /// <summary>
        /// 批量插入数据
        /// </summary>
        /// <param name="entitys">获取的实体类型集合</param>
        void InsertBulk(IEnumerable<T> entitys);

        /// <summary>
        /// 用于插入数据(异步)
        /// </summary>
        /// <param name="entity">获取的实体类型</param>
        /// <returns></returns>
        Task InserAsync(T entity);

        /// <summary>
        /// 批量插入数据(异步)
        /// </summary>
        /// <param name="entitys">获取的实体类型集合</param>
        /// <returns></returns>
        Task InserAsyncBulk(IEnumerable<T> entitys);

        /// <summary>
        /// 删除对应数据
        /// </summary>
        /// <param name="Id">获取的id</param>
        void Deleted(object Id);

        /// <summary>
        /// 批量删除对应数据
        /// </summary>
        /// <param name="Ids">获取的id集合</param>
        void DeletedBulk(IEnumerable<object> Ids);

        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="entity">获取的id</param>
        void Updated(T entity);

        /// <summary>
        /// 批量更新数据
        /// </summary>
        /// <param name="entitys">获取的id集合</param>
        void UpdatedBulk(IEnumerable<T> entitys);
    }
}