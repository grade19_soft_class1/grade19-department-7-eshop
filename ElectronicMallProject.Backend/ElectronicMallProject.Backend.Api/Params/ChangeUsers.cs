namespace ElectronicMallProject.Backend.Api.Params
{
    public class ChangeUsers
    {
        public string UserName { get; set; }
        public string PassWord { get; set; }
    }
}