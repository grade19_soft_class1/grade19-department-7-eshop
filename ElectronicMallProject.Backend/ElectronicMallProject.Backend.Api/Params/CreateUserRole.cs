namespace ElectronicMallProject.Backend.Api.Params
{
    public class CreateUserRole
    {
        public string UserRole { get; set; }
    }
}