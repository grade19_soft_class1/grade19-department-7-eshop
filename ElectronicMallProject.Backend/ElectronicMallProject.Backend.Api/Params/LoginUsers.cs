namespace ElectronicMallProject.Backend.Api.Params
{
    public class LoginUsers
    {
        public string UserName { get; set; }
        public string PassWord { get; set; }
    }
}