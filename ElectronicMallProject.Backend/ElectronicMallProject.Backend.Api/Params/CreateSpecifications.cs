namespace ElectronicMallProject.Backend.Api.Entity
{
    //修改规格
    public class CreateSpecifications
    {
        public string SpecificationsName { get; set; }
        public int CommodityId { get; set; }
        public int price { get; set; }
    }
}