using System;

namespace ElectronicMallProject.Backend.Api.Entity
{
    public class ChangeSales
    {
        //订单号
        public string SalesNumber{get;set;}
        //日期
        public string Date{get;set;}
        //单个商品物件数
        public int NumberOfSingleProductItems{get;set;}
        //总物件数
        public int TotalNumberOfObjects{get;set;}
        //单笔金额
        public int SingleAmount{get;set;}
        //总金额
        public int TotalMoney{get;set;}
        //支付方式
        public string PaymentMethod{get;set;}
        //用户Id
        public int UserId{get;set;}
        //商品Id
        public int CommodityId{get;set;}
        //物流信息
        public string LogisticsInformation{get;set;}
        //发货时间
        public string DeliveryTime{get;set;}
        //订单状态
        public string SalesStatus{get;set;}
        //支付状态
        public string PaymentStatus{get;set;}
        //实际付款金额
        public int PaymentAmount{get;set;}
        //买家状态
        public string BuyerStatus{get;set;}
    }
}