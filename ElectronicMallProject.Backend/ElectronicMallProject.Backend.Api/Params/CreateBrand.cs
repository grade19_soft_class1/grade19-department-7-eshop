namespace ElectronicMallProject.Backend.Api.Entity
{
    //品牌
    public class CreateBrand
    {
        //品牌表
        public string BrandName { get; set; }
        //品牌logo图id
        public int logoImgId { get; set; }
        //品牌封面图id
        public int coverImgId { get; set; }
    }
}