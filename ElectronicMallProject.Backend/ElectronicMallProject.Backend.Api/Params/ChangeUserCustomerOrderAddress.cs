namespace ElectronicMallProject.Backend.Api.Params
{
    public class ChangeUserCustomerOrderAddress
    {
        public string CustomerOrderAddress{get;set;}
    }
}