namespace ElectronicMallProject.Backend.Api.Params
{
    public class RefreshTokenDTO
    {
        public string Token { get; set; } 
        public string refreshToken { get; set; }
    }
}