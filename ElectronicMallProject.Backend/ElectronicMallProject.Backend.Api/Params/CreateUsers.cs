namespace ElectronicMallProject.Backend.Api.Params
{
    public class CreateUsers
    {
        public string UserName { get; set; }
        public string PassWord { get; set; }
        public string RePassword { get; set; }
        public string UserRole { get; set; }
    }
}