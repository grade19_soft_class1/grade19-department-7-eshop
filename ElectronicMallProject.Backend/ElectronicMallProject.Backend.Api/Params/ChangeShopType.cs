
namespace ElectronicMallProject.Backend.Api.Entity
{
    public class ChangeShopType
    {
        //商店类型名
        public string ShopTypeName {get;set;}

        //上级分类
        public int SuperiorShopTypeId {get;set;}

        //计量单位
        public string Measure {get;set;}

        //商店类型图片Id
        public int ShopTypeIconId{get;set;}

        //筛选属性
        public string FilterProperties{get;set;}

        //关键词        
        public string  keyword {get;set;}
    }
}