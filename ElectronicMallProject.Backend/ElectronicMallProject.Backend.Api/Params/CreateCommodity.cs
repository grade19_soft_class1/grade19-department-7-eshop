namespace ElectronicMallProject.Backend.Api.Entity
{
   public class CreateCommodity
   {
       public string CommodityName { get; set; }
       public string CommodityType { get; set; }
       public int CommodityCurrentPrice { get; set; }
       public int SpecificationsId { get; set; }
       public string ProductDetails { get; set; }
       public int ProductStocks { get; set; }
       public int ImgId { get; set; }
       public int SupplierId { get; set; }
       public int BrandId { get; set; }
       public int ShopTypeId { get; set; }
   } 
}  