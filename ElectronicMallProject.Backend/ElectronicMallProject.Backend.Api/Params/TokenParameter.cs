namespace ElectronicMallProject.Backend.Api.Params
{
    public class TokenParameter
    {
        //生成token的密钥
        public string Secret { get; set; }

        //签发人的名称
        public string Issuer { get; set; }

        //Token的有效分钟数
        public int AccessExpiration { get; set; }

        // refreshToken的有效分钟数
        public int RefreshExpiration { get; set; }
    }
}