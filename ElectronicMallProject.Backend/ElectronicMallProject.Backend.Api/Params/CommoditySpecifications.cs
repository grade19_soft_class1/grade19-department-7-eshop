namespace ElectronicMallProject.Backend.Api.Params
{
    public class CommoditySpecifications
    {
        // 规格名
        public string SpecificationsName { get; set; }
        //商品Id
        public int CommodityId { get; set; }
        //价格
        public int Price { get; set; }
    }
}