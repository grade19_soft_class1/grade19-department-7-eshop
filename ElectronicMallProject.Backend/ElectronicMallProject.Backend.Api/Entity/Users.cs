namespace ElectronicMallProject.Backend.Api.Entity
{
    public class Users:BaseInit
    {
        //用户名
        public string UserName{get;set;}
        //密码
        public string PassWord{get;set;}
        //手机号
        public string PhoneNumber{get;set;}
        //地址
        public string Address{get;set;}
        //订单地址
        public string CustomerOrderAddress{get;set;}
        //用户角色
        public string UserRole{get;set;}
    }
}