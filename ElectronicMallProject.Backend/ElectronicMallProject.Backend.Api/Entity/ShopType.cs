
namespace ElectronicMallProject.Backend.Api.Entity
{
    //商品类型
    public class ShopType : BaseInit
    {
        //商店类型名
        public string ShopTypeName {get;set;}

        //上级分类
        public int SuperiorShopTypeId {get;set;}

        //数量
        public string Measure {get;set;}

        //商店类型图片Id
        public int ShopTypeIconId{get;set;}

        //删选属性
        public string FilterProperties{get;set;}

        //关键词        
        public string  keyword {get;set;}
    }
}