namespace ElectronicMallProject.Backend.Api.Entity
{
    public class Shop:BaseInit
    {
        //店铺名称
        public string ShopName{get;set;}
        //商品类型id  
        public int ShopTypeId{get;set;}
    }
}