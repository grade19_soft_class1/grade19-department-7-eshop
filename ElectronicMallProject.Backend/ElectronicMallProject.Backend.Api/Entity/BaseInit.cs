using System;

namespace ElectronicMallProject.Backend.Api.Entity
{
    public abstract class BaseInit
    {
        public int Id {get;set;}
        public bool IsActived {get;set;}
        public bool IsDeleted {get;set;}
        public DateTime CreatedTime {get;set;}
        public DateTime UpdatedTime {get;set;}
        public int DisplayOrder {get;set;}
        public string Remarks {get;set;}
    }
}