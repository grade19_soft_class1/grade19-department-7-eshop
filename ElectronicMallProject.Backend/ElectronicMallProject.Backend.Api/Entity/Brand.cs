namespace ElectronicMallProject.Backend.Api.Entity
{
    //品牌
    public class Brand : BaseInit
    {
        //品牌表
        public string BrandName { get; set; }
        //品牌logo图id
        public int logoImgId { get; set; }
    }
}

