namespace ElectronicMallProject.Backend.Api.Entity
{
    //用户信息
    public class UserInfo : BaseInit
    {
        //用户Id
        public int UserId { get; set; }

        //图片Id
        public int ImgId { get; set; }

        // 证件信息
        public string CardType { get; set; }

        //身份证号
        public string CardNumber { get; set; }

        //性别
        public char Sex { get; set; }

        //手机号
        public string phoneNumber { get; set; }
    }
}