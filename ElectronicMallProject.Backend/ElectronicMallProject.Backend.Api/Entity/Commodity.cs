namespace ElectronicMallProject.Backend.Api.Entity
{
    public class Commodity : BaseInit
    {
        //商品名称
        public string CommodityName { get; set; }
        //商品类型
        public string CommodityType { get; set; }
        //商品原价
        public int CommodityOriginalPrice { get; set; }
        //商品当前售价
        public int CommodityCurrentPrice { get; set; }
        //商品品级
        public int CommodityGrade { get; set; }
        //商品规格Id
        public int SpecificationsId { get; set; }
        //商品详情
        public string ProductDetails { get; set; }
        //商品库存
        public int ProductStocks { get; set; }
        //封面图Id
        public int ImgId { get; set; }
        //供应商Id
        public int SupplierId { get; set; }
        //商品品牌Id
        public int BrandId { get; set; }
        //商品类型Id
        public int ShopTypeId { get; set; }
    }
}