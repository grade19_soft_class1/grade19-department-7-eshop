namespace ElectronicMallProject.Backend.Api.Entity
{
    public class UserRole : BaseInit
    {
        public string Admin { get; set; }
        public string Business { get; set; }
        public string Users { get; set; }
        public string Supplier { get; set; }
    }
}