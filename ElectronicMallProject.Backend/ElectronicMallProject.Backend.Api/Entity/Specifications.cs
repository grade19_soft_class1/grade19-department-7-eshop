namespace ElectronicMallProject.Backend.Api.Entity
{
    public class Specifications : BaseInit
    {
        // 规格名
        public string SpecificationsName { get; set; }
        //商品Id
        public int CommodityId { get; set; }
        //价格
        public int price { get; set; }
    }
}