namespace ElectronicMallProject.Backend.Api.Entity
{
    public class Supplier:BaseInit
    {
        //供应商名字
        public string SupplierName{get;set;}
        //供应商地址
        public string SupplierAddress{get;set;}
        //联系人
        public string Contacts{get;set;}
        //供应商营业
        public string MainBusiness{get;set;}
    }
}