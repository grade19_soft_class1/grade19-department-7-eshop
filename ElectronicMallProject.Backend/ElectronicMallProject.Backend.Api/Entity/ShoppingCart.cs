namespace ElectronicMallProject.Backend.Api.Entity
{
    public class ShoppingCart:BaseInit
    {
        //商品Id
        public int CommodityId{get;set;}
        //用户Id
        public int UserId { get; set; }
        //规格Id
        public int SpecificationsId { get; set; }
        //数量
        public int CommdityNumber { get; set; }
    }
}