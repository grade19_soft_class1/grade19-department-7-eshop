namespace ElectronicMallProject.Backend.Api.Entity
{
    public class Comment:BaseInit
    {
        //评论内容
        public string Content{get;set;}
        //评论用户Id
        public int ComUserId{get;set;}
        //回复用户Id
        public int ReplyUserId{get;set;}
        //商品展示栏Id
        public int CommodityDisplayColumnId{get;set;}
    }
}