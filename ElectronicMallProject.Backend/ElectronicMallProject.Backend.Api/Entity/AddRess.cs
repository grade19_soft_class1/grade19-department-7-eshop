using ElectronicMallProject.Backend.Api.Entity;

namespace ElectronicMallProject.Backend.Api.Database
{

    //收件地址表
    public class AddRess : BaseInit
    {
        //省
        public string Province { get; set; }
        //市
        public string City { get; set; }
        //区
        public string District { get; set; }
        //街道
        public string Street { get; set; }
        //详细地址
        public string Detailedaddress { get; set; }
        //号码
        public string Number { get; set; }
        //收件人
        public string Addressee { get; set; }
    }
}