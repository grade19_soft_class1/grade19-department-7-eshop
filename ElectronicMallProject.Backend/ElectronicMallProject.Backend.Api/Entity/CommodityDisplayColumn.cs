namespace ElectronicMallProject.Backend.Api.Entity
{
    public class CommodityDisplayColumn:BaseInit
    {
        //展示栏名
        public string DisplayBar{get;set;}
        //商铺Id
        public int ShopId{get;set;}
    }
}