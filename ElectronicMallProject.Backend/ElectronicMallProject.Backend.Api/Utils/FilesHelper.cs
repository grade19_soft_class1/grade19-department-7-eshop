using System;
using System.IO;
using ElectronicMallProject.Backend.Api.Entity;
using ElectronicMallProject.Backend.Api.Repository;
using Microsoft.AspNetCore.Hosting;

namespace ElectronicMallProject.Backend.Api.Controllers
{
    public class FilesHelper
    {
        public static dynamic UpImgs(dynamic files, IWebHostEnvironment env, IRepository<Imgs> imgsRepository)
        {
            var _imgsRespostisory = imgsRepository;
            var imgsRootPath = env.ContentRootPath;
            string filesPath = "";

            string physicPath = imgsRootPath + "/files/";  //物理路径   

            //判断文件是否存在
            if (!Directory.Exists(physicPath))
            {
                Directory.CreateDirectory(physicPath);
            }

            foreach (var formFile in files)
            {
                
                    //文件名字
                    string filesName = DateTime.Now.ToFileTime() + Path.GetExtension(formFile.FileName);
                    //绝对路径
                    string fileFullPath = Path.Combine(physicPath, filesName);
                    using (var stream = new FileStream(fileFullPath, FileMode.CreateNew))
                    {
                        formFile.CopyTo(stream);//保存文件
                    }
                    filesPath += fileFullPath + ',';
                
            }

            filesPath = filesPath.TrimEnd(',');
            return filesPath;
        }
    }
}