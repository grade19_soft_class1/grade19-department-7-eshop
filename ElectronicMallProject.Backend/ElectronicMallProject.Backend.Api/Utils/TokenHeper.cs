
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using ElectronicMallProject.Backend.Api.Params;
using ElectronicMallProject.Backend.Api.Entity;

namespace ElectronicMallProject.Backend.Api.Utils
{
    public class TokenHelper
    {
        //这儿是真正的生成Token代码
        public static string GenUserToken(TokenParameter tokenParameter, string user)
        {
            var claims = new[]
            {
                new Claim(ClaimTypes.Name, user),
                new Claim(ClaimTypes.Role, "admin"),
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tokenParameter.Secret));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var jwtToken = new JwtSecurityToken(tokenParameter.Issuer, null, claims, expires: DateTime.UtcNow.AddMinutes(tokenParameter.AccessExpiration), signingCredentials: credentials);

            var token = new JwtSecurityTokenHandler().WriteToken(jwtToken);

            return token;
        }

        /// <summary>
        /// 验证token并从中得到用户名
        /// </summary>
        /// <param name="tokenParameter">token设置</param>
        /// <param name="refresh">刷新token的模型，里面是token和refreshToken</param>
        public static string ValidateToken(TokenParameter tokenParameter, RefreshTokenDTO refresh)
        {
            //这儿是验证Token的代码
            var handler = new JwtSecurityTokenHandler();
            try
            {
                ClaimsPrincipal claim = handler.ValidateToken(refresh.Token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(tokenParameter.Secret)),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidateLifetime = false,
                }, out SecurityToken securityToken);

                return claim.Identity.Name;

            }
            catch
            {
                return null;
            }
        }
    }
}