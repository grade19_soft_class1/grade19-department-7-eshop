using Microsoft.AspNetCore.Mvc;
using System.Linq;
using ElectronicMallProject.Backend.Api.Entity;
using Microsoft.Extensions.Configuration;
using ElectronicMallProject.Backend.Api.Repository;


//规格控制器
namespace ElectronicMallProject.Backend.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SpecificationsController : ControllerBase
    {
        private IRepository<Specifications> _specificationsRepository;
        private IConfiguration _configuration;

        public SpecificationsController(IRepository<Specifications> specificationsRepository, IConfiguration configuration)
        {
            _specificationsRepository = specificationsRepository;
            _configuration = configuration;
        }

        //查询所有规格
        [HttpGet, Route("getnofrozen")]
        public dynamic Getnofrozen()
        {
            var specifications = _specificationsRepository.Table;

            return new
            {
                Code = 200,
                Data = specifications,
                Msg = "查询规格成功"
            };
        }

        //查询所有未冻结规格规格
        [HttpGet]
        public dynamic Get()
        {
            var specifications = _specificationsRepository.Table.Where(x => x.IsActived.Equals(true)).ToList();

            return new
            {
                Code = 200,
                Data = specifications,
                Msg = "查询规格成功"
            };
        }

        //添加规格
        [HttpPost, Route("Addspecifications")]
        public dynamic AddPost(CreateSpecifications specifications)
        {
            var specificationsname = specifications.SpecificationsName.Trim();
            var commodityid = specifications.CommodityId;

            if (string.IsNullOrEmpty(specificationsname) && string.IsNullOrEmpty(commodityid.ToString()))
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "规格名称不能为空"
                };
            }

            var Addspecifications = _specificationsRepository.Table;

            // var ishavespecifications = Addspecifications.Where(x => x.SpecificationsName.Equals(specificationsname)).ToList();

            var res = new Specifications
            {
                SpecificationsName = specificationsname,
                CommodityId = commodityid
            };

            _specificationsRepository.Insert(res);

            return new
            {
                Code = 200,
                Data = res,
                Msg = "添加规格成功"
            };

        }

        //修改规格
        [HttpPut, Route("{id}/updatespecifications")]
        public dynamic UpdatePut(int id, CreateSpecifications specifications)
        {
            var specificationsname = specifications.SpecificationsName.Trim();
            var commodityid = specifications.CommodityId;
            var price = specifications.price;

            if (!string.IsNullOrEmpty(specificationsname) && !string.IsNullOrEmpty(commodityid.ToString()) && !string.IsNullOrEmpty(price.ToString()))
            {
                var updatespecifications = _specificationsRepository.GetById(id);

                if (updatespecifications == null)
                {
                    return new
                    {
                        Code = 200,
                        Data = "",
                        Msg = "要修改的规格不存在"
                    };
                }

                updatespecifications.SpecificationsName = specifications.SpecificationsName;
                updatespecifications.CommodityId = specifications.CommodityId;
                updatespecifications.price = specifications.price;

                _specificationsRepository.Updated(updatespecifications);

                return new
                {
                    Code = 200,
                    Data = updatespecifications,
                    Msg = "修改规格成功"
                };
            }
            else
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "修改规格失败"
                };
            }

        }

        //冻结规格
        [HttpPut, Route("{id}/frozenspecifications")]
        public dynamic frozenPut(int id)
        {
            var frozenspecifications = _specificationsRepository.GetById(id);

            if (frozenspecifications == null)
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "该规格不存在"
                };
            }

            frozenspecifications.IsActived = false;

            _specificationsRepository.Updated(frozenspecifications);

            return new
            {
                Code = 200,
                Data = frozenspecifications,
                Msg = "冻结规格成功"
            };
        }

        //启用规格
        [HttpPut, Route("{id}/enablespecifications")]
        public dynamic enablePut(int id)
        {
            var enablespecifications = _specificationsRepository.GetById(id);

            if (enablespecifications == null)
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "该规格不存在"
                };
            }

            enablespecifications.IsActived = true;

            _specificationsRepository.Updated(enablespecifications);

            return new
            {
                Code = 200,
                Data = enablespecifications,
                Msg = "启用规格成功"
            };
        }
    }
}