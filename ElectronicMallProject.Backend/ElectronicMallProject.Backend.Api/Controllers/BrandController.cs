using Microsoft.AspNetCore.Mvc;
using ElectronicMallProject.Backend.Api.Entity;
using ElectronicMallProject.Backend.Api.Repository;
using System.Linq;
using ElectronicMallProject.Backend.Api.Params;

namespace ElectronicMallProject.Backend.Api.Controllers
{
    //品牌控制器
    [ApiController]
    [Route("[controller]")]
    public class BrandController : ControllerBase
    {
        private IRepository<Brand> _brandRepository;

        public BrandController(IRepository<Brand> brandRepository)
        {
            _brandRepository = brandRepository;
        }


        //查看所有品牌
        [HttpGet, Route("BrandGet")]
        public dynamic brandGet()
        {
            var brand = _brandRepository.Table;

            return new
            {
                Code = 200,
                Data = brand,
                Msg = "获取所有品牌成功"
            };
        }

        //添加品牌
        [HttpPost, Route("AddBrand")]
        public dynamic AddPost(CreateBrand brand)
        {
            var brandname = brand.BrandName.Trim();
            var logoimgid = brand.logoImgId;
            var coverimgid = brand.coverImgId;

            if (string.IsNullOrEmpty(brandname))
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "品牌名不能为空"
                };
            }

            var addbrand = _brandRepository.Table;

            var ishavebrandname = addbrand.Where(x => x.BrandName.Equals(brandname)).ToList();

            if (ishavebrandname.Count() == 0)
            {
                var res = new Brand
                {
                    BrandName = brandname,
                    logoImgId = logoimgid
                };

                _brandRepository.Insert(res);

                return new
                {
                    Code = 200,
                    Data = res,
                    Msg = "添加品牌成功"
                };
            }
            else
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "该品牌已存在"
                };
            }
        }

        //修改品牌
        [HttpPut, Route("{id}/updatebrand")]
        public dynamic updateBrandPut(int id, CreateBrand brand)
        {
            var brandname = brand.BrandName.Trim();
            var logoimgid = brand.logoImgId;
            var coverimgid = brand.coverImgId;

            var updatebrand = _brandRepository.GetById(id);

            if (updatebrand == null)
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "修改品牌不存在"
                };
            }

            updatebrand.BrandName = brand.BrandName;
            updatebrand.logoImgId = brand.logoImgId;

            _brandRepository.Updated(updatebrand);

            return new
            {
                Code = 200,
                Data = updatebrand,
                Msg = "修改品牌成功"
            };
        }

        //冻结商品类型
        [HttpPut, Route("{id}/frozenbrand")]
        public dynamic frozenPut(int id)
        {
            var frozenbrand = _brandRepository.GetById(id);

            if (frozenbrand == null)
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "该品牌不存在"
                };
            }
            frozenbrand.IsActived = false;
            _brandRepository.Updated(frozenbrand);

            return new
            {
                Code = 200,
                Data = frozenbrand,
                Msg = "冻结品牌成功"
            };
        }

        //是否启用商品类型
        [HttpPut, Route("{id}/enablebrand")]
        public dynamic enablePut(int id)
        {
            var enablebrand = _brandRepository.GetById(id);

            if (enablebrand == null)
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "该品牌不存在"
                };
            }
            enablebrand.IsActived = true;
            _brandRepository.Updated(enablebrand);

            return new
            {
                Code = 200,
                Data = enablebrand,
                Msg = "启用品牌成功"
            };
        }


        //查询品牌类型
        [HttpPost,Route("queryBrand")]
        public dynamic queryBrand(Myquery query)
        {
            var querydata = query.queryData;
            var Brand = _brandRepository.Table.Where(x =>x.BrandName.Contains(querydata)).ToList();


            return new {
                code = 200,
                data = Brand,
                msg = "查询成功"
            };
        }
    }
}