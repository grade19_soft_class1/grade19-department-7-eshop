using Microsoft.AspNetCore.Mvc;
using ElectronicMallProject.Backend.Api.Repository;
using ElectronicMallProject.Backend.Api.Entity;
using Microsoft.AspNetCore.Hosting;
using System.Linq;

namespace ElectronicMallProject.Backend.Api.Controllers
{
    //文件上传控制器
    [ApiController]
    [Route("[controller]")]
    public class FileUploadProcessingController : ControllerBase
    {
        private readonly IRepository<Imgs> _imgsRepository;
        private readonly IWebHostEnvironment _iWebHostEnvironment;

        public FileUploadProcessingController(IRepository<Imgs> imgsRepository, IWebHostEnvironment iWebHostEnvironment)
        {
            _imgsRepository = imgsRepository;
            _iWebHostEnvironment = iWebHostEnvironment;
        }

        //文件上传
        [HttpPost, Route("FileUpload")]
        public dynamic FileUpload()
        {
            var file = Request.Form.Files;
            var filePath = FilesHelper.UpImgs(file, _iWebHostEnvironment, _imgsRepository);

            var sql = new Imgs
            {
                ImgsPath = filePath
            };
            _imgsRepository.Insert(sql);

            return sql;
        }

        //查看图片
        [HttpGet, Route("imgs")]
        public dynamic Getimgs()
        {
            var imgs = _imgsRepository.Table;

            return new
            {
                imgs
            };
        }

        //查看未禁用的图片
        [HttpGet]
        public dynamic GetDynamic()
        {
            var imgs = _imgsRepository.Table.Where(x => x.IsActived.Equals(true)).ToList();

            return new
            {
                Code = 200,
                Data = imgs,
                Msg = "查询成功"
            };
        }


        //冻结图片
        [HttpPut, Route("{id}/frozenimgs")]
        public dynamic frozenPut(int id)
        {
            var frozenimgs = _imgsRepository.GetById(id);

            if (frozenimgs == null)
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "该图片不存在"
                };
            }

            frozenimgs.IsActived = false;

            _imgsRepository.Updated(frozenimgs);

            return new
            {
                Code = 200,
                Data = frozenimgs,
                Msg = "冻结图片成功"
            };
        }

        //启用图片
        [HttpPut, Route("{id}/enableimgs")]
        public dynamic enablePut(int id)
        {
            var enableimgs = _imgsRepository.GetById(id);

            if (enableimgs == null)
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "该图片不存在"
                };
            }

            enableimgs.IsActived = true;

            _imgsRepository.Updated(enableimgs);

            return new
            {
                Code = 200,
                Data = enableimgs,
                Msg = "启用图片成功"
            };
        }
    }
}