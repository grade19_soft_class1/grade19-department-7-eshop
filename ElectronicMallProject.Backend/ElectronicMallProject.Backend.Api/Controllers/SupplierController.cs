using System.Linq;
using ElectronicMallProject.Backend.Api.Entity;
using ElectronicMallProject.Backend.Api.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ElectronicMallProject.Backend.Api.Params;

//供应商控制器
namespace ElectronicMallProject.Backend.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SupplierController : ControllerBase
    {
        //供应商表
        private IRepository<Specifications> _specificationsRepository;
        private IConfiguration _configuration;
        public SupplierController(IRepository<Specifications> specificationsRepository, IConfiguration configuration)
        {
            _specificationsRepository = specificationsRepository;

            _configuration = configuration;
        }

        //获取所有供应商
        [HttpGet, Route("Get")]
        public dynamic Get()
        {
            var specifications = _specificationsRepository.Table;
            return new
            {
                Code = 200,
                Data = specifications,
                Msg = "获取所有供应商成功"
            };
        }

        //添加供应商
        [HttpPost, Route("Addspecifications")]
        public dynamic SpecificationsPost(CreateSpecifications specifications)
        {
            var specificationsname = specifications.SpecificationsName.Trim();

            if (string.IsNullOrEmpty(specificationsname))
            {
                return new
                {
                    Code = 104,
                    Data = specificationsname,
                    Msg = "请输入供应商"
                };
            }

            var Specifications = _specificationsRepository.Table;
            var ishaveSpecifications = Specifications.Where(x => x.SpecificationsName.Equals(specificationsname)).ToList();

            if (ishaveSpecifications.Count() == 0)
            {
                var res = new Specifications
                {
                    SpecificationsName = specificationsname
                };
                _specificationsRepository.Insert(res);

                return new
                {
                    Code = 200,
                    Data = res,
                    Msg = "添加供应商成功"
                };
            }

            return "";
        }

        //修改供应商
        [HttpPut, Route("{id}/Changespecifications")]
        public dynamic Put(int id, ChangeSpecifications change)
        {
            var specificationsname = change.SpecificationsName.Trim();

            if (!string.IsNullOrEmpty(specificationsname))
            {
                var specifications = _specificationsRepository.GetById(id);

                if (specifications == null)
                {
                    return new
                    {
                        Code = 104,
                        Data = "",
                        Msg = "修改供应商不存在"
                    };
                }

                specifications.SpecificationsName = change.SpecificationsName;

                _specificationsRepository.Updated(specifications);

                return new
                {
                    Code = 200,
                    Data = specifications,
                    Msg = "修改供应商成功"
                };
            }
            else
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "修改供应商失败"
                };
            }
        }


        //冻结供应商
        [HttpPut, Route("{id}/Frozenspecifications")]
        public dynamic frozenPut(int id)
        {
            var specifications = _specificationsRepository.GetById(id);

            if (specifications == null)
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "该供应商不存在"
                };
            }
            specifications.IsActived = false;
            _specificationsRepository.Insert(specifications);

            return new
            {
                Code = 200,
                Data = specifications,
                Msg = "冻结用户成功"
            };
        }

        //启用供应商
        [HttpPut, Route("{id}/Enablespecifications")]
        public dynamic enablePut(int id)
        {
            var specifications = _specificationsRepository.GetById(id);

            if (specifications == null)
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "该供应商不存在"
                };
            }
            specifications.IsActived = true;
            _specificationsRepository.Updated(specifications);

            return new
            {
                Code = 200,
                Data = specifications,
                Msg = "启用供应商成功"
            };
        }
    }
}