using Microsoft.AspNetCore.Mvc;
using ElectronicMallProject.Backend.Api.Entity;
using ElectronicMallProject.Backend.Api.Repository;
using System.Collections;
using System.Linq;

namespace ElectronicMallProject.Backend.Api.Controllers
{
    //轮播图控制器
    [ApiController]
    [Route("[controller]")]

    public class CarouselsController : ControllerBase
    {
        private readonly IRepository<Carousels> _carouselsRepository;
        private readonly IRepository<Imgs> _imgsRepository;

        public CarouselsController(IRepository<Carousels> carouselsRepository, IRepository<Imgs> imgsRepository)
        {
            _carouselsRepository = carouselsRepository;
            _imgsRepository = imgsRepository;
        }

        [HttpGet, Route("imgget")]
        public dynamic imgGet()
        {
            var carousels = _carouselsRepository.Table.ToList();
            var imgs = _imgsRepository.Table;

            ArrayList arr = new ArrayList();

            foreach (var item in carousels)
            {
                var img = imgs.Where(x => x.Id.Equals(item.ImgsId)).FirstOrDefault();

                var tmp = new
                {
                    id = item.Id,
                    CommodityId = item.CommodityId,
                    ImgsPath = img != null ? img.ImgsPath : ""
                };
                arr.Add(tmp);
            }

            return new
            {
                Code = 200,
                Data = arr,
                Msg = "获取轮播图成功"
            };
        }
    }
}