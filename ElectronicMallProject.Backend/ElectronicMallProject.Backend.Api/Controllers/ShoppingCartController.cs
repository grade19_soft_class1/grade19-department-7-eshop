using System.Linq;
using ElectronicMallProject.Backend.Api.Repository;
using Microsoft.Extensions.Configuration;
using ElectronicMallProject.Backend.Api.Entity;
using Microsoft.AspNetCore.Mvc;
using ElectronicMallProject.Backend.Api.Params;
using System.Collections;
using Microsoft.EntityFrameworkCore;

//购物车表
namespace ElectronicMallProject.Backend.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ShoppingCartController : ControllerBase
    
    {
        //图片表
        private IRepository<Imgs> _imgRepository;
        //购物车
        private IRepository<ShoppingCart> _shoppingcartRepository;
        //规格表
        private IRepository<Specifications> _specificationsRepository;
        //商品表
        private IRepository<Commodity> _commodityRepository;
        //品牌表
        private IRepository<Brand> _brandRepository;
        //类型表
        private IRepository<ShopType> _shoptypeRepository;
        private IConfiguration _configuration;

        public ShoppingCartController(IRepository<Imgs> imgRepository,IRepository<ShopType> shoptypeRepository,IRepository<Brand> brandRepository,
        IRepository<ShoppingCart> shoppingcartRepository, IConfiguration configuration ,
        IRepository<Specifications> specificationsRepository ,IRepository<Commodity> commodityRepository)
        {
            _imgRepository = imgRepository;
            _shoptypeRepository = shoptypeRepository;
            _brandRepository = brandRepository;
            _shoppingcartRepository = shoppingcartRepository;            
            _configuration = configuration;
            _specificationsRepository = specificationsRepository;
            _commodityRepository = commodityRepository;
        }

        //查询所有购物车
        [HttpGet]
        public dynamic Get()
        {
            var shoppingcart = _shoppingcartRepository.Table;

            return new
            {
                Card = 200,
                Data = shoppingcart,
                Msg = "查询购物车成功"
            };
        }

        //通过用户Id后去该用户购物车记录
        [HttpGet("{id}/uid")]
        public dynamic getByUid(int id)
        {
            var shop = _shoppingcartRepository.Table.Where(x => x.UserId == id).ToList();

            return new
            {
                code = 200,
                data = shop,
                msg = "请求成功"
            };
        }

        //通过用户Id查找该用户购物车记录同时获取商品表,类型表,品牌表
        [HttpGet,Route("{id}/getuserId")]
        public dynamic UserIdGet(int id)
        {
            var shoppingCart = _shoppingcartRepository.Table.Where(x =>x.UserId ==id).ToList();
            ArrayList list = new ArrayList();
            foreach (var item in shoppingCart)
            {
                var specificationsName = _specificationsRepository.Table.Where(x =>x.Id == item.SpecificationsId).FirstOrDefault();
                var commoditys = _commodityRepository.Table.Where(x =>x.Id == item.CommodityId).ToList();
                foreach (var commodity in commoditys)
                {
                    var ShopType = _shoptypeRepository.Table.Where(x =>x.Id == commodity.ShopTypeId).FirstOrDefault();
                    var brand = _brandRepository.Table.Where(x=>x.Id == commodity.BrandId).FirstOrDefault();
                    var specification = _specificationsRepository.Table.Where(x=>x.CommodityId == commodity.Id).ToList();
                    var img = _imgRepository.Table.Where(x =>x.Id == commodity.ImgId).FirstOrDefault();
                    

                    var res = new
                    {
                        Id= commodity.Id,
                        CommodityName = commodity.CommodityName,
                        IsActived = commodity.IsActived,
                        IsDeleted = commodity.IsDeleted,
                        Remarks = commodity.Remarks,
                        UpdatedTime = commodity.UpdatedTime,
                        CommodityCurrentPrice = commodity.CommodityCurrentPrice,
                        ShopType=ShopType.ShopTypeName,
                        brand = brand.BrandName,
                        imgpath = img.ImgsPath,
                        specification = specificationsName,
                        shoppingId = item.Id
                        
                    };
                    list.Add(res);
                }
            }
            return new
            {
                code = 200,
                data = list,
                msg = "请求成功"
            };
        }
 

        //通过用户Id查找该用户购物车记录同时获取商品表信息
        [HttpGet("{id}")]
        public dynamic getByUserId(int id)
        {
            var shoppingcart = _shoppingcartRepository.Table.Where(x => x.UserId == id).ToList();


            ArrayList list = new ArrayList();

            foreach (var item in shoppingcart)
            {
                var commoditys = _commodityRepository.Table.Where(x => x.Id == item.CommodityId).ToList();

                foreach (var commodity in commoditys)
                {
                    var specification = _specificationsRepository.Table.Where(x =>x.Id == commodity.SpecificationsId).ToList();
                    var res = new {
                        Id= commodity.Id,
                        CommodityName = commodity.CommodityName,
                        IsActived = commodity.IsActived,
                        IsDeleted = commodity.IsDeleted,
                        Remarks = commodity.Remarks,
                        UpdatedTime = commodity.UpdatedTime,
                        CommodityCurrentPrice = commodity.CommodityCurrentPrice,
                        SpecificationsId = specification
                    };
                    list.Add(res);
                }
                // var res = new
                // {
                //     Id = item.Id,
                //     CommodityId = item.CommodityId,
                //     UserId = item.UserId,
                //     IsActived = item.IsActived,
                //     IsDeleted = item.IsDeleted,
                //     Remarks = item.Remarks,
                //     UpdatedTime = item.UpdatedTime,
                //     children = children,
                // };
                
            }
            return new
            {
                code = 200,
                data = list,
                msg = "请求成功"
            };
        }

        //添加到购物车
        [HttpPost, Route("Addshoppingcart")]
        public dynamic AddPost(CreateShoppingCart ShoppingCart)
        {
            var CommodityId = ShoppingCart.CommodityId;
            var UserId = ShoppingCart.UserId;
            var SpecificationsId = ShoppingCart.SpecificationsId;
            var CommdityNumber = ShoppingCart.CommdityNumber;

            if (string.IsNullOrEmpty(CommodityId.ToString()) && string.IsNullOrEmpty(UserId.ToString()) && string.IsNullOrEmpty(SpecificationsId.ToString()) && string.IsNullOrEmpty(CommdityNumber.ToString()))
            {
                return new
                {
                    Card = 104,
                    Data = "",
                    Msg = "请填写完整"
                };
            }

            var shoppingcart = _shoppingcartRepository.Table;
            var commoditys =_commodityRepository.Table;

            // var ishaveCommodityId = commoditys.Where(x => x.Id.Equals(CommodityId)).ToList();

            // if (ishaveCommodityId.Count() == 0)
            // {
                var res = new ShoppingCart
                {
                    CommodityId = CommodityId,
                    UserId = UserId,
                    SpecificationsId= SpecificationsId,
                    CommdityNumber = CommdityNumber
                };

                _shoppingcartRepository.Insert(res);

                return new
                {
                    Code = 200,
                    Data = res,
                    Msg = "添加购物车成功"
                };
            // }
            // return new
            // {
            //     Code = 104,
            //     Data = "",
            //     Msg = "该购物车已存在"
            // };

        }

        //修改购物车
        [HttpPut, Route("{id}/Updateshoppingcart")]
        public dynamic UpdatePut(int id, CreateShoppingCart shoppingCart)
        {
            var CommodityId = shoppingCart.CommodityId;
            var UserId = shoppingCart.UserId;
            var SpecificationsId = shoppingCart.SpecificationsId;
            var CommdityNumber = shoppingCart.CommdityNumber;

            if (string.IsNullOrEmpty(CommodityId.ToString()) && string.IsNullOrEmpty(UserId.ToString()) && string.IsNullOrEmpty(SpecificationsId.ToString()) && string.IsNullOrEmpty(CommdityNumber.ToString()))
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "请填写完整"
                };
            }

            var updateshoppingcart = _shoppingcartRepository.GetById(id);

            if (updateshoppingcart == null)
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "找不到该购物车"
                };
            }

            updateshoppingcart.CommodityId = shoppingCart.CommodityId;
            updateshoppingcart.UserId = shoppingCart.UserId;
            updateshoppingcart.SpecificationsId = shoppingCart.SpecificationsId;
            updateshoppingcart.CommdityNumber = shoppingCart.CommdityNumber;

            _shoppingcartRepository.Updated(updateshoppingcart);

            return new
            {
                Code = 200,
                Data = updateshoppingcart,
                Msg = "修改购物车成功"
            };
        }

        //冻结购物车
        [HttpPut, Route("{id}/frozenshoppingcart")]
        public dynamic frozenPut(int id)
        {
            var frozenshoppingcart = _shoppingcartRepository.GetById(id);

            if (frozenshoppingcart == null)
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "该购物车不存在"
                };
            }
            frozenshoppingcart.IsActived = false;
            _shoppingcartRepository.Updated(frozenshoppingcart);

            return new
            {
                Code = 200,
                Data = frozenshoppingcart,
                Msg = "冻结购物车成功"
            };
        }

        //启用购物车
        [HttpPut, Route("{id}/enableshoppingcart")]
        public dynamic enablePut(int id)
        {
            var enableshoppingcart = _shoppingcartRepository.GetById(id);

            if (enableshoppingcart == null)
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "该购物车不存在"
                };
            }
            enableshoppingcart.IsActived = true;
            _shoppingcartRepository.Updated(enableshoppingcart);

            return new
            {
                Code = 200,
                Data = enableshoppingcart,
                Msg = "启用购物车成功"
            };
        }

    }
}