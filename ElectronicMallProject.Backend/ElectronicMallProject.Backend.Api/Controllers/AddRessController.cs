using ElectronicMallProject.Backend.Api.Database;
using ElectronicMallProject.Backend.Api.Entity;
using ElectronicMallProject.Backend.Api.Repository;
using Microsoft.AspNetCore.Mvc;
using ElectronicMallProject.Backend.Api.Params;

namespace ElectronicMallProject.Backend.Api.Controllers
{
    //地址控制器
    [ApiController]
    [Route("[controller]")]

    public class AddRessController:ControllerBase
    {
        private readonly IRepository<AddRess> _addressRepository;

        public AddRessController(IRepository<AddRess> addressRepository)
        {
            _addressRepository = addressRepository;
        }


        //获取地址表数据
        [HttpGet]
        public dynamic getAddRess()
        {
            var address = _addressRepository.Table;

            return new
            {
                code = 200,
                data = address,
                msg = "请求成功"
            };
        }

        //添加地址表数据
        [HttpPost]
        public dynamic postAddRess(CreateAddRess createAddRess)
        {
            //省
            var province = createAddRess.Province.Trim();

            //市
            var city = createAddRess.City.Trim();

            //区
            var district = createAddRess.District.Trim();

            //街道
            var street = createAddRess.Street.Trim();

            //详细地址
            var detailedaddress = createAddRess.Detailedaddress.Trim();

            //号码
            var number = createAddRess.Number.Trim();

            //收件人
            var addressee = createAddRess.Addressee.Trim();


            var address = _addressRepository.Table;

            var res = new AddRess
            {
                Province = province,
                City = city,
                District = district,
                Street = street,
                Detailedaddress = detailedaddress,
                Number = number,
                Addressee = addressee
            };

            _addressRepository.Insert(res);

            return new
            {
                code = 200,
                data = "",
                msg = "添加成功"
            };
        }


        //修改地址表信息
        [HttpPut("{id}")]
        public dynamic putAddRess(int id , ChangeAddRess changeAddRess)
        {
            var address = _addressRepository.GetById(id);

            //省
            var Province = changeAddRess.Province.Trim();

            //市
            var city = changeAddRess.City.Trim();

            //区
            var district = changeAddRess.District.Trim();

            //街道
            var street = changeAddRess.Street.Trim();

            //详细地址
            var detailedaddress = changeAddRess.Detailedaddress.Trim();

            //号码
            var number = changeAddRess.Number.Trim();

            //收件人
            var addressee = changeAddRess.Addressee.Trim();



            address.Province = changeAddRess.Province;
            address.City = changeAddRess.City;
            address.District = changeAddRess.District;
            address.Street = changeAddRess.Street;
            address.Detailedaddress = changeAddRess.Detailedaddress;
            address.Number = changeAddRess.Number;
            address.Addressee = changeAddRess.Addressee;

            _addressRepository.Updated(address);

            return new
            {
                code = 200,
                data = address,
                msg = "修改成功"
            };
        }


        //冻结信息
        [HttpPut("{id}/freeze")]
        public dynamic freezeAddRess(int id)
        {
            var address = _addressRepository.GetById(id);

            if (address == null)
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "该记录不存在"
                };
            }
            address.IsActived = false;
            _addressRepository.Updated(address);

            return new
            {
                Code = 200,
                Data = address,
                Msg = "冻结成功"
            };
        }

        //启用
        [HttpPut("{id}/enable")]
        public dynamic enableAddRess(int id)
        {
            var address = _addressRepository.GetById(id);

            if (address == null)
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "该记录不存在"
                };
            }
            address.IsActived = true;
            _addressRepository.Updated(address);

            return new
            {
                Code = 200,
                Data = address,
                Msg = "启用成功"
            };
        }

    }
}