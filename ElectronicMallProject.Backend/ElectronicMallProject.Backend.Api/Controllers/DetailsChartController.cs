using System.Linq;
using ElectronicMallProject.Backend.Api.Entity;
using ElectronicMallProject.Backend.Api.Repository;

//商品详情图
using Microsoft.AspNetCore.Mvc;

namespace ElectronicMallProject.Backend.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DetailsChartController : ControllerBase
    {
        //商品表
        private IRepository<Commodity> _commodityRepository;
        //图片表
        private IRepository<Imgs> _imgRepository;
        //详情表
        private IRepository<DetailsChart> _detailschartRepository;

        public DetailsChartController(IRepository<DetailsChart> detailschartRepository)
        {
            _detailschartRepository = detailschartRepository;
        }

        [HttpGet]
        public dynamic Get()
        {
            var detailschart = _detailschartRepository.Table;
            return new
            {
                Code = 200,
                Data = detailschart,
                Msg = "查询所有商品成功"
            };
        }


        //通过商品Id查看商品以及详细图片
        // [HttpGet,Route("{id}/comimgget")]
        // public dynamic comimgGet(int id)
        // {
        //     var commodity = _commodityRepository.Table.Where(x=>x.Id == id).FirstOrDefault();


        // }

        
    }

}