
using ElectronicMallProject.Backend.Api.Repository;
using Microsoft.AspNetCore.Mvc;
using ElectronicMallProject.Backend.Api.Entity;
using ElectronicMallProject.Backend.Api.Params;
using System.Linq;
using System.Collections;

namespace ElectronicMallProject.Backend.Api.Controllers
{

    //销售控制器
    [ApiController]
    [Route("[controller]")]
    public class SalesController : ControllerBase
    {
        private readonly IRepository<Sales> _salesRepository;
        private readonly IRepository<Commodity> _commodityRepository;
        private readonly IRepository<Users> _usersRepository;
        private readonly IRepository<ShoppingCart> _shoppingcartRepository;

        public SalesController(IRepository<Sales> salesRepository, IRepository<Commodity> commodityRepository, IRepository<Users> usersRepository, IRepository<ShoppingCart> shoppingResitory)

        {
            _salesRepository = salesRepository;
            _commodityRepository = commodityRepository;
            _usersRepository = usersRepository;
            _shoppingcartRepository = shoppingResitory;
        }


        //获取表数据
        [HttpGet]
        public dynamic getSales()
        {
            var sales = _salesRepository.Table;
            return new
            {
                code = 200,
                data = sales,
                msg = "请求成功"
            };
        }

        //查询订单
        [HttpPost,Route("querySales")]
        public dynamic querySales(Myquery query)
        {
            var queryData = query.queryData.Trim();
            var sales = _salesRepository.Table.Where(x =>x.SalesNumber.Contains(queryData)).ToList();

            return new
            {
                code = 200,
                data = sales,
                msg = "查询成功"
            };
        }

        //获取某用户所产生的订单
        [HttpGet, Route("{id}/orderGet")]
        public dynamic orderGet(int id)
        {
            var orderuser = _salesRepository.Table.Where(x => x.UserId == id).ToList();

            return new
            {
                code = 200,
                data = orderuser,
                msg = "获取相应用户订单表成功 "
            };
        }


        //订单表商品表连接查询
        [HttpGet("{id}")]
        public dynamic GetCom(int id)
        {

            var uu = _salesRepository.Table.Where(x => x.UserId == id).ToList();

            ArrayList list = new ArrayList();
            foreach (var item in uu)
            {
                var children = _commodityRepository.Table.Where(x => x.Id == item.CommodityId).FirstOrDefault();
                var otherUserId = _shoppingcartRepository.Table.Where(x => x.UserId != id).FirstOrDefault();

                var otherId = otherUserId.UserId;

                var res = new
                {
                    Id = item.Id,
                    SalesNumber = item.SalesNumber,
                    Date = item.Date,
                    NumberOfSingleProductItems = item.NumberOfSingleProductItems,
                    TotalNumberOfObjects = item.TotalNumberOfObjects,
                    SingleAmount = item.SingleAmount,
                    TotalMoney = item.TotalMoney,
                    PaymentMethod = item.PaymentMethod.Trim(),
                    UserId = item.UserId,
                    CommodityId = item.CommodityId,
                    LogisticsInformation = item.LogisticsInformation.Trim(),
                    DeliveryTime = item.DeliveryTime,
                    SalesStatus = item.SalesStatus.Trim(),
                    PaymentStatus = item.PaymentStatus.Trim(),
                    PaymentAmount = item.PaymentAmount,
                    BuyerStatus = item.BuyerStatus,
                    IsActived = item.IsActived,
                    IsDeleted = item.IsDeleted,
                    Remarks = item.Remarks,
                    UpdatedTime = item.UpdatedTime,
                    CommodityName = children.CommodityName,
                    CommodityCurrentPrice = children.CommodityCurrentPrice,
                    otherUserId = otherId
                };
                list.Add(res);
            }

            // var result = from commodity in _commodityRepository join sales in _salesRepository 
            // on commodity.id equals sales.CommodityId select new (commodity , sales);

            return new
            {
                Code = 200,
                Data = list,
                Msg = "请求成功"
            };
        }


        //添加
        [HttpPost]
        public dynamic postSales(CreateSales createSales)
        {
            //订单号
            var salesNumber = createSales.SalesNumber.Trim();
            //日期
            var date = createSales.Date;
            //单个商品物件数
            var numberOfSingleProductItems = createSales.NumberOfSingleProductItems;
            //总物件数
            var totalNumberOfObjects = createSales.TotalNumberOfObjects;
            //单笔金额
            var singleAmount = createSales.SingleAmount;
            //总金额
            var totalMoney = createSales.TotalMoney;
            //支付方式
            var paymentMethod = createSales.PaymentMethod.Trim();
            //用户Id
            var userId = createSales.UserId;
            //商品Id
            var commodityId = createSales.CommodityId;
            //物流信息
            var logisticsInformation = createSales.LogisticsInformation.Trim();
            //发货时间
            var deliveryTime = createSales.DeliveryTime;
            //订单状态
            var salesStatus = createSales.SalesStatus.Trim();
            //支付状态
            var paymentStatus = createSales.PaymentStatus.Trim();
            //实际付款金额
            var paymentAmount = createSales.PaymentAmount;
            //买家状态
            var buyerStatus = createSales.BuyerStatus;

            var addSales = new Sales
            {
                SalesNumber = salesNumber,
                Date = date,
                NumberOfSingleProductItems = numberOfSingleProductItems,
                TotalNumberOfObjects = totalNumberOfObjects,
                SingleAmount = singleAmount,
                TotalMoney = totalMoney,
                PaymentMethod = paymentMethod,
                UserId = userId,
                CommodityId = commodityId,
                LogisticsInformation = logisticsInformation,
                DeliveryTime = deliveryTime,
                SalesStatus = salesStatus,
                PaymentStatus = paymentStatus,
                PaymentAmount = paymentAmount,
                BuyerStatus = buyerStatus,
            };
            _salesRepository.Insert(addSales);

            return new
            {
                code = 200,
                data = "",
                msg = "添加成功"
            };
        }


        [HttpPut("{id}")]
        public dynamic putSales(int id, ChangeSales changesales)
        {
            var salesForId = _salesRepository.GetById(id);

            //订单号
            var SalesNumber = changesales.SalesNumber.Trim();
            //日期
            var Date = changesales.Date;
            //单个商品物件数
            var NumberOfSingleProductItems = changesales.NumberOfSingleProductItems;
            //总物件数
            var TotalNumberOfObjects = changesales.TotalNumberOfObjects;
            //单笔金额
            var SingleAmount = changesales.SingleAmount;
            //总金额
            var TotalMoney = changesales.TotalMoney;
            //支付方式
            var PaymentMethod = changesales.PaymentMethod.Trim();
            //用户Id
            var UserId = changesales.UserId;
            //商品Id
            var CommodityId = changesales.CommodityId;
            //物流信息
            var LogisticsInformation = changesales.LogisticsInformation.Trim();
            //发货时间
            var DeliveryTime = changesales.DeliveryTime;
            //订单状态
            var SalesStatus = changesales.SalesStatus.Trim();
            //支付状态
            var PaymentStatus = changesales.PaymentStatus.Trim();
            //实际付款金额
            var PaymentAmount = changesales.PaymentAmount;
            //买家状态
            var BuyerStatus = changesales.BuyerStatus;


            salesForId.SalesNumber = changesales.SalesNumber;
            salesForId.Date = changesales.Date;
            salesForId.NumberOfSingleProductItems = changesales.NumberOfSingleProductItems;
            salesForId.TotalNumberOfObjects = changesales.TotalNumberOfObjects;
            salesForId.SingleAmount = changesales.SingleAmount;
            salesForId.TotalMoney = changesales.TotalMoney;
            salesForId.PaymentMethod = changesales.PaymentMethod;
            salesForId.UserId = changesales.UserId;
            salesForId.CommodityId = changesales.CommodityId;
            salesForId.LogisticsInformation = changesales.LogisticsInformation;
            salesForId.DeliveryTime = changesales.DeliveryTime;
            salesForId.SalesStatus = changesales.SalesStatus;
            salesForId.PaymentAmount = changesales.PaymentAmount;
            salesForId.BuyerStatus = changesales.BuyerStatus;


            _salesRepository.Updated(salesForId);

            return new
            {
                code = 200,
                data = salesForId,
                msg = "修改成功"
            };
        }


        //冻结销售信息
        [HttpPut("{id}/freeze")]
        public dynamic freezeSales(int id)
        {
            var sales = _salesRepository.GetById(id);

            if (sales == null)
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "该记录不存在"
                };
            }
            sales.IsActived = false;
            _salesRepository.Updated(sales);

            return new
            {
                Code = 200,
                Data = sales,
                Msg = "冻结成功"
            };
        }


        [HttpPut("{id}/enable")]
        public dynamic enableSales(int id)
        {
            var sales = _salesRepository.GetById(id);

            if (sales == null)
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "该记录不存在"
                };
            }
            sales.IsActived = true;
            _salesRepository.Updated(sales);

            return new
            {
                Code = 200,
                Data = sales,
                Msg = "启用成功"
            };
        }

    }
}


