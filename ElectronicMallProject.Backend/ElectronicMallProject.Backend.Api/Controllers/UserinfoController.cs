using ElectronicMallProject.Backend.Api.Entity;
using ElectronicMallProject.Backend.Api.Repository;
using Microsoft.AspNetCore.Mvc;
using ElectronicMallProject.Backend.Api.Params;

namespace ElectronicMallProject.Backend.Api.Controllers
{
    //用户信息控制器
    [ApiController]
    [Route("[controller]")]
    public class UserinfoController : ControllerBase
    {
        private readonly IRepository<UserInfo> _userInfoRepository;

        public UserinfoController(IRepository<UserInfo> userInfoRepository)
        {
            _userInfoRepository = userInfoRepository;
        }


        //获取用户信息表
        [HttpGet]
        public dynamic getUserInfo()
        {
            var userInfo = _userInfoRepository.Table;

            return new
            {
                code = 200,
                data = userInfo,
                msg = "请求成功"
            };
        }

        [HttpPost]
        public dynamic postUserInfo(CreateUserInfo createUserInfo)
        {
            var userId = createUserInfo.UserId;
            var imgId = createUserInfo.ImgId;
            var cardType = createUserInfo.CardType;
            var cardNumber = createUserInfo.CardNumber;
            var sex = createUserInfo.Sex;
            var phoneNum = createUserInfo.phoneNumber;

            var res = new UserInfo
            {
                UserId = userId,
                ImgId = imgId,
                CardType = cardType,
                CardNumber = cardNumber,
                Sex = sex,
                phoneNumber = phoneNum
            };


            _userInfoRepository.Insert(res);

            return new
            {
                code = 200,
                data = "",
                msg = "添加成功"
            };
        }

        [HttpPut("{id}")]
        public dynamic putUserInfo(int id,ChangeUserInfo changeUserInfo)
        {
            var UserId = changeUserInfo.UserId;
            var ImgId = changeUserInfo.ImgId;
            var CardType = changeUserInfo.CardType;
            var CardNumber = changeUserInfo.CardNumber;
            var Sex = changeUserInfo.Sex;
            var phoneNumber = changeUserInfo.phoneNumber;

            var ui = _userInfoRepository.GetById(id);

            ui.UserId = changeUserInfo.UserId;
            ui.ImgId = changeUserInfo.ImgId;
            ui.CardType = changeUserInfo.CardType;
            ui.CardNumber = changeUserInfo.CardNumber;
            ui.Sex = changeUserInfo.Sex;
            ui.phoneNumber = changeUserInfo.phoneNumber;

            _userInfoRepository.Updated(ui);

            return new
            {
                code = 200,
                data = ui,
                msg = "修改成功"
            };
        }


        //冻结用户信息
        [HttpPut("{id}/freeze")]
        public dynamic freezeUserInfo(int id)
        {
            var userInfo = _userInfoRepository.GetById(id);

            if (userInfo == null)
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "该用户信息不存在"
                };
            }
            userInfo.IsActived = false;
            _userInfoRepository.Updated(userInfo);

            return new
            {
                Code = 200,
                Data = userInfo,
                Msg = "冻结用户成功"
            };
        }


        [HttpPut("{id}/enable")]
        //启用用户信息
        public dynamic enableUserInfo(int id)
        {
            var userInfo = _userInfoRepository.GetById(id);

            if (userInfo == null)
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "该用户信息不存在"
                };
            }
            userInfo.IsActived = true;
            _userInfoRepository.Updated(userInfo);

            return new
            {
                Code = 200,
                Data = userInfo,
                Msg = "启用用户成功"
            };
        }
    }
}