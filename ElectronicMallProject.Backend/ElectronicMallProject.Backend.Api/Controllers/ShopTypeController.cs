using Microsoft.AspNetCore.Mvc;
using System.Linq;
using ElectronicMallProject.Backend.Api.Entity;
using Microsoft.Extensions.Configuration;
using ElectronicMallProject.Backend.Api.Repository;
using System.Collections;

//商品类型控制器
namespace ElectronicMallProject.Backend.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ShopTypeController : ControllerBase
    {
        //商品类型表
        private IRepository<ShopType> _shoptyperepository;
        private IConfiguration _configuration;

        public ShopTypeController(IRepository<ShopType> shoptyperepository, IConfiguration configuration)
        {
            _shoptyperepository = shoptyperepository;
            _configuration = configuration;
        }


        //获取所有商品类型
        [HttpGet, Route("get")]
        public dynamic Get()
        {
            var shop = _shoptyperepository.Table;
            return new
            {
                Code = 200,
                Data = shop,
                Msg = "获取所有商品类型成功"
            };
        }

        //通过id获取商品类型
        [HttpGet, Route("{id}/getshop")]
        public dynamic shopGet(int id)
        {
            var shop = _shoptyperepository.GetById(id);

            return new
            {
                Code = 200,
                Data = shop,
                Msg = "获取所有商品类型成功"
            };
        }

        //添加商品类型
        [HttpPost, Route("addshop")]
        public dynamic addPost(ChangeShopType shopType)
        {
            var shoptypename = shopType.ShopTypeName.Trim();
            var superiorshoptypeid = shopType.SuperiorShopTypeId;
            var measure = shopType.Measure.Trim();
            var shoptypeiconid = shopType.ShopTypeIconId;
            var filterproperties = shopType.FilterProperties.Trim();
            var keyword = shopType.keyword.Trim();

            if (string.IsNullOrEmpty(shoptypename) && string.IsNullOrEmpty(superiorshoptypeid.ToString()))
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "请填写完整"
                };
            }

            var shoptype = _shoptyperepository.Table;

            var ishaveshoptypename = shoptype.Where(x => x.ShopTypeName.Equals(shoptypename)).ToList();

            if (ishaveshoptypename.Count() == 0)
            {
                var res = new ShopType
                {
                    ShopTypeName = shoptypename,
                    SuperiorShopTypeId = superiorshoptypeid,
                    Measure = measure,
                    ShopTypeIconId = shoptypeiconid,
                    FilterProperties = filterproperties,
                    keyword = keyword
                };

                _shoptyperepository.Insert(res);

                return new
                {
                    Code = 200,
                    Data = res,
                    Msg = "添加商品类型成功"
                };
            }

            return new
            {
                Code = 104,
                Data = "",
                Msg = "该商品类型已存在"
            };

        }

        //修改商品类型
        [HttpPut, Route("{id}/updateshop")]
        public dynamic updateshopPut(int id, ChangeShopType shopType)
        {
            var shoptypename = shopType.ShopTypeName.Trim();
            var superiorshoptypeid = shopType.SuperiorShopTypeId;
            var measure = shopType.Measure.Trim();
            var shoptypeiconid = shopType.ShopTypeIconId;
            var filterproperties = shopType.FilterProperties.Trim();
            var keyword = shopType.keyword.Trim();

            if (!string.IsNullOrEmpty(shoptypename) && !string.IsNullOrEmpty(superiorshoptypeid.ToString()) )
            {
                var updateshop = _shoptyperepository.GetById(id);

                if (updateshop == null)
                {
                    return new
                    {
                        Code = 104,
                        Data = "",
                        Msg = "修改商品类型不存在"
                    };
                }

                updateshop.ShopTypeName = shopType.ShopTypeName;
                updateshop.SuperiorShopTypeId = shopType.SuperiorShopTypeId;
                updateshop.Measure = shopType.Measure;
                updateshop.FilterProperties = shopType.FilterProperties;
                updateshop.keyword = shopType.keyword;

                _shoptyperepository.Updated(updateshop);

                return new
                {
                    Code = 200,
                    Data = updateshop,
                    Msg = "修改商品类型成功"
                };
            }
            else
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "修改商品类型失败"
                };
            }

        }

        //冻结商品类型
        [HttpPut, Route("{id}/frozenshop")]
        public dynamic frozenPut(int id)
        {
            var frozenshop = _shoptyperepository.GetById(id);

            if (frozenshop == null)
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "该商品类型不存在"
                };
            }
            frozenshop.IsActived = false;
            _shoptyperepository.Updated(frozenshop);

            return new
            {
                Code = 200,
                Data = frozenshop,
                Msg = "冻结商品类型成功"
            };
        }

        //是否启用商品类型
        [HttpPut, Route("{id}/enableshop")]
        public dynamic enablePut(int id)
        {
            var enableshop = _shoptyperepository.GetById(id);

            if (enableshop == null)
            {
                return new
                {
                    Code = 104,
                    Data = "",
                    Msg = "该商品类型不存在"
                };
            }
            enableshop.IsActived = !enableshop.IsActived;
            _shoptyperepository.Updated(enableshop);

            return new
            {
                Code = 200,
                Data = enableshop,
                Msg = "启用商品类型成功"
            };
        }

        //通过商品id查询上一级和本级
        [HttpGet,Route("{id}")]
        public dynamic GradeGet(int id)
        {
            
            var shop = _shoptyperepository.Table;
            var Grade = _shoptyperepository.Table.Where(x=>x.SuperiorShopTypeId == id).ToList();
            ArrayList list = new ArrayList();
            foreach (var item in Grade)
            {
                var children = _shoptyperepository.Table.Where(x=>x.SuperiorShopTypeId == item.Id).ToList();
                var res  = new {
                    Id = item.Id,
                    SuperiorShopTypeId = item.SuperiorShopTypeId,
                    ShopTypeName= item.ShopTypeName,
                    ShopTypeIconId = item.ShopTypeIconId,
                    IsActived = item.IsActived,
                    IsDeleted = item.IsDeleted,
                    keyword = item.keyword,
                    Measure = item.Measure,
                    Remarks = item.Remarks,
                    UpdatedTime = item.UpdatedTime,
                    children = children
                };
            list.Add(res);
            }
            
            return new
            {
                Code = 200,
                Data = list,
                Msg = "查询成功"
            };
            
        }

        //获取所有商品类型 + Children
        [HttpGet, Route("getchildren")]
        public dynamic childrenGet()
        {
            var shop = _shoptyperepository.Table;
            ArrayList list = new ArrayList();
            ArrayList children = new ArrayList();
            foreach(var item in shop)
            {
                var res = new
                {
                    Id = item.Id,
                    shopTypeName = item.ShopTypeName,
                    superiorShopTypeId = item.SuperiorShopTypeId,
                    shopTypeIconId = item.ShopTypeIconId,
                    filterProperties = item.FilterProperties,
                    keyword = item.keyword,
                    isActived = item.IsActived,
                    isDeleted = item.IsDeleted,
                    createdTime = item.CreatedTime,
                    updatedTime = item.UpdatedTime,
                    displayOrder = item.DisplayOrder,
                    remarks =item.Remarks,
                    children = children
                };
                list.Add(res);
            }
            
            return new
            {
                Code = 200,
                Data = list,
                Msg = "获取所有商品类型成功"
            };
        }
    }

}