# 19级软件1班7部EShop项目
# 电子商城管理系统
## 技术栈
    1.前后端分离技术
    2.前端采用Vue（vue-cli、vue-router、axios）、Element UI、Echarts、yarn包管理器等
    3.后端采用Asp.Net core3.1 WebApi（dotnet命令行工具、Entity Framework Core、依赖注入、代码优先、数据迁移、种子数据）、MSSqlserver（2014 Express版）、JWT、Newtosoft序列化工具、Rest Clientapi测试工具等
    4.开发工具：Visual Studio Code，前端后端均采用vscode进行开发
    5.代码版本管理工具：git
    6.代码托管平台：gitee 码云
    7.webpack模块打包器: 通过webpack能够将14MB的文件压缩至4.8MB加快加载速度

## 使用说明
## 前端ElectronicMallProject.Frontend
    1.确保Node.js是10.4版本以上
    2.项目拉取以后确认终端目录进入ElectronicMallProject.Frontend 
    3.yarn 安装依赖
    4.yarn serve 运行项目
    5.(如果要部署到生产环境)请执行yarn build 命令，打包项目，打包好的项目位于ElectronicMallProject.Frontend 下的dist目录，将dist目录下的所有文件上传至服务器，使用nginx等web服务器进行配置部署 压缩文件已经写好

## 后端ElectronicMallProject.Backend(请确保已经安装dotnet jdk3.1,并且已经安装了postgresql数据库，并修改appsettings.json中的数据库连接)
    1.确保Node.js是10.4版本以上
    2.项目拉取以后确认终端目录进入ElectronicMallProject.Backend
    3.执行dotnet restore 命令，还原项目
    4.dotnet ef database update 命令，迁移数据到数据库（同时生成数据库和数据表，如果没有ef 命令，请使用dotnet tool install --global dotnet-ef命令）
    5.执行dotnet run命令执行（或者按F5进行调试执行，需要配置调试文件）
    6.如果需要部署请执行dotnet publish -c release 进行发布 将release上传服务器

## 项目截图如下：
### 所有商品信息

![图片出错啦](./电子商城文档/08项目截图/所有商品信息.png)

### 品牌管理

![图片出错啦](./电子商城文档/08项目截图/品牌管理.png)

### 用户列表

![图片出错啦](./电子商城文档/08项目截图/用户列表.png)

### 订单列表

![图片出错啦](./电子商城文档/08项目截图/订单列表.png)

### 商品详情页

![图片出错啦](./电子商城文档/08项目截图/商品详情页.png)



#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
