



# Linux+Docker操作系统部署项目（参考课时：4课时）

# 一、需求描述

​		本章节主要教学生如何在Linux操作系统中部署.NET Core项目，需要学生提前Linux操作系统和Docker有一定的了解。

# 二、知识点

1. 项目发布

2. Linux操作系统。
3. Docker容器技术。

# 三、重点和难点

1. Linux操作系统。
2. Docker。


# 四、授课思路

​		根据要求完成授课即可，务必要让学生熟练项目部署流程，可以不定期进行项目部署测试。

# 五、具体步骤

# 一.Dockerfile方式部署 .Net Core3.1项目
## 1.为.NetCore项目添加docker支持。
&emsp;&emsp;方式1：

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210224162701123.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMzMTAxNjg5,size_16,color_FFFFFF,t_70#pic_center)
&emsp;&emsp;方式2：

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210224162719647.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMzMTAxNjg5,size_16,color_FFFFFF,t_70#pic_center)
&emsp;&emsp;添加docker支持后，vs会自动帮我们生成dockerfile文件

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210224162739802.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMzMTAxNjg5,size_16,color_FFFFFF,t_70#pic_center)
&emsp;&emsp;
## 2.部署.NetCore项目
&emsp;&emsp;
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210224162750599.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMzMTAxNjg5,size_16,color_FFFFFF,t_70#pic_center)
&emsp;&emsp;
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210224162757674.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMzMTAxNjg5,size_16,color_FFFFFF,t_70#pic_center)
&emsp;&emsp;
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210224162811167.png#pic_center)
&emsp;&emsp;
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210224162820625.png#pic_center)
&emsp;&emsp;
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210224163048223.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMzMTAxNjg5,size_16,color_FFFFFF,t_70)

&emsp;&emsp;注意：Net5部署时，需要改一下dockerfile
&emsp;&emsp;注意：dockfile文件的存放位置要特别注意。如果dockerfile跟vs生成的目录一致，则需要修改命令，映射路径。

# 三. 总结

&emsp;&emsp;本次教程使用的是.NET Core WebAPI进行部署，.NET Core MVC也是一样的。

