



# Linux操作系统部署项目（参考课时：4课时）

# 一、需求描述

​		本章节主要教学生如何在Linux操作系统中部署.NET Core项目，需要学生提前Linux操作系统和Nginx有一定的了解。

# 二、知识点

1. 项目发布

2. Linux操作系统。
3. Nginx。

# 三、重点和难点

1. Linux操作系统。
2. Nginx。


# 四、授课思路

​		根据要求完成授课即可，务必要让学生熟练项目部署流程，可以不定期进行项目部署测试。

# 五、具体步骤

# 一. 前期准备


&emsp;&emsp;1.服务器准备：全新华为云，CentOS 7.6 64bit，（开放端口安全组，防火墙关闭，如果有未使用的硬盘可以挂载硬盘（先分区->格式化->挂载文件目录），网卡设置（华为云不需要））
&emsp;&emsp;2.软件准备：远端电脑的终极工具箱MobaXterm，其他的当然也可以，例如Xshell，Xftp等
&emsp;&emsp;3.已发布好的.Net Core项目		

# 二. 开搞
## 1. 安装.Net Core环境

&emsp;&emsp;**1.首先我们打开微软官网，安装需要安装的包** [https://dotnet.microsoft.com/download](https://dotnet.microsoft.com/download).
&emsp;&emsp;
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200904145909299.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMzMTAxNjg5,size_16,color_FFFFFF,t_70#pic_center)
&emsp;&emsp;
&emsp;&emsp;**2. 点击后进入新页面，找到CentOS**
&emsp;&emsp;
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200904145917641.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMzMTAxNjg5,size_16,color_FFFFFF,t_70#pic_center)
&emsp;&emsp;
&emsp;&emsp;**3.点击后进入新页面，找到CentOS7，执行微软提供的这两条指令，耐心等待即可**
&emsp;&emsp;
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200904145930573.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMzMTAxNjg5,size_16,color_FFFFFF,t_70#pic_center)
&emsp;&emsp;
![在这里插入图片描述](https://img-blog.csdnimg.cn/2020090414594571.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMzMTAxNjg5,size_16,color_FFFFFF,t_70#pic_center)
&emsp;&emsp;
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200904145957188.png#pic_center)
&emsp;&emsp;
&emsp;&emsp;**4. 安装完成后，输入命令：dotnet --version 查看版本，输入命令：dotnet new --help查看安装的模块**
&emsp;&emsp;
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200904152459131.png#pic_center)
&emsp;&emsp;
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200904152511711.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMzMTAxNjg5,size_16,color_FFFFFF,t_70#pic_center)
&emsp;&emsp;**到这里，dotnet环境安装大功告成！**
&emsp;&emsp;
&emsp;&emsp;



## 2. 安装Nginx并启动Nginx

&emsp;&emsp;
&emsp;&emsp;**1.把提前下载好的nginx（[nginx下载路径http://nginx.org/en/download.html，注意别下windows版本](http://nginx.org/en/download.html)）拉到远程linux的 /ldg/myapp 目录中（该目录事先有了一个nginx文件夹，是用于安装nginx的目录），并执行解压命令tar -xzvf nginx-1.18.0.tar.gz** 
&emsp;&emsp;

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200904110330579.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMzMTAxNjg5,size_16,color_FFFFFF,t_70#pic_center)
&emsp;&emsp;


![在这里插入图片描述](https://img-blog.csdnimg.cn/20200904110502752.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMzMTAxNjg5,size_16,color_FFFFFF,t_70#pic_center)
&emsp;&emsp;
&emsp;&emsp;**2.进入解压好的文件夹目录 /ldg/myapp/nginx-1.18.0，安装各种依赖库,命令：yum install -y gcc gcc-c++ automake pcre pcre-devel zlib zlib-devel openssl openssl-devel** 
&emsp;&emsp;
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200904110537303.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMzMTAxNjg5,size_16,color_FFFFFF,t_70#pic_center)
&emsp;&emsp;
&emsp;&emsp;**5.配置nginx的安装路径。 输入命令：./configure --prefix=/ldg/myapp/nginx （如果有关于logs的报错，说明 nginx/目录下没有logs文件夹，这个时候创建一个logs文件夹即可）**


&emsp;&emsp;**6.启动Nginx,进入sbin目录，输入指令：./nginx**

&emsp;&emsp;**7.如果需要配置nginx开机自启动，则输入指令：vim /etc/rc.d/rc.local**
&emsp;&emsp;**按照下图修改即可。当然，还可以有其他办法-----------**
&emsp;&emsp;
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200904143928338.png#pic_center)
&emsp;&emsp;
&emsp;&emsp;**保存退出时可能会报错：E45: 'readonly' option is set (add ! to override) ，是因为文件权限问题，这时候退出编辑模式输入冒号后，输入 wq!即可强制保存退出。**


&emsp;&emsp;**8.外网访问测试nginx**
&emsp;&emsp;

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200904141406867.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMzMTAxNjg5,size_16,color_FFFFFF,t_70#pic_center)

nginx其他命令 ： ./nginx -s reload（重启nginx）
	


&emsp;&emsp;&emsp;&emsp;
&emsp;&emsp;
&emsp;&emsp;**提示：** 如果仍然无法打开网页，可以把linux防火墙，把它关闭，或者开启nginx配置的端口。方法如下：
&emsp;&emsp;查看防火墙状态：  firewall-cmd --state 
&emsp;&emsp;关闭防火墙： systemctl stop firewalld.service  （打开时，把stop改成start）
  &emsp;&emsp;关闭防火墙开机自启动：systemctl disable firewalld.service 如果显示not running，则关闭成功
## 3. 启动nginx和.Net Core项目，开始测试
  &emsp;&emsp;**1.首先我们先修改linux系统中的配置文档(配置地址直接为真实ip)，配置负载均衡策略，定位到sbin目录，输入命令：./nginx -s reload（重启nginx）**
    &emsp;&emsp;
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200905105617713.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMzMTAxNjg5,size_16,color_FFFFFF,t_70#pic_center)

  &emsp;&emsp;
  &emsp;&emsp;**2.把已发布好的项目，先配置好启动端口（注意，建议用配置文档的方式，而且地址为 http://0.0.0.0:端口号，在linux中直接命令配置端口号反正我是启动不成功的）**
    &emsp;&emsp;
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200905104407852.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMzMTAxNjg5,size_16,color_FFFFFF,t_70#pic_center)
  &emsp;&emsp;

  &emsp;&emsp;**3.应该后台启动，在使用dotnet xxx.dll运行项目时，如断开shell之后将会被终止服务。所以可以执行命令：nohup 你需要执行得命令 &(注意：&符号别漏了！)**
    &emsp;&emsp;
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200905104525601.png#pic_center)
  &emsp;&emsp;
  &emsp;&emsp;**4.输入命令：ps sux 查看进程即可看到项目已经运行， 如果想关闭某个进程，就输入命令： kill 进程号**
   &emsp;&emsp;
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200905104540187.png#pic_center)
&emsp;&emsp;
  &emsp;&emsp;**5.结果展示：现在部署了3个后台webapi进程，使用nginx权重模式负载均衡测试成功**
  &emsp;&emsp;
![在这里插入图片描述](https://img-blog.csdnimg.cn/20200905105403987.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMzMTAxNjg5,size_16,color_FFFFFF,t_70#pic_center)
&emsp;&emsp;

&emsp;&emsp;那么这个是在开发环境，如何在产线部署呢。可以以守护进程supervisor启动程序，在supervisor的启动配置里面增加环境变量。
# 三. 总结

&emsp;&emsp;本次教程使用的是.NET Core WebAPI进行部署，.NET Core MVC也是一样的，后续进阶可以使用docker容器技术进行部署。

