import request from "../utils/request";

export function getShopType() {
    return request.get(`ShopType/get`)
}

export function addShopType(data) {
    return request.post(`shopType/addshop`, data)
}

export function updateShopTypeById(id, data) {
    return request.put(`shoptype/${id}/updateshop`, data)
}

export function getShopTypeById(id) {
    return request.get(`shoptype/${id}/getshop`)
}