import request from "../utils/request";

export function sales() {
    return request({ url: "/sales", method: "get" });
}
//查询订单
export function querySales(data) {
    return request.post(`sales/querySales`, data);
}