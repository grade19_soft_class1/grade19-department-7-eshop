import request from "../utils/request";

export function Commodity() {
    return request.get(`Commodity`);
}

export function frozencommodity(id) {
    return request.put(`commodity/${id}/frozencommodity`);
}

export function enablecommodity(id) {
    return request.put(`commodity/${id}/enablecommodity`);
}

export function AddCommodity(CommodityData) {
    return request.post(`Commodity/Addcommodity`, CommodityData)
}
export function AddCommodityimgs(CommodityimgsData) {
    return request.post(`Commodity/Commodityimgs`, CommodityimgsData);
}
export function AddSpecifications(SpecificationData) {
    return request.post(`Commodity/Specifications`, SpecificationData)
}

//通过商品Id查找商品
export function getCommodityById(id) {
    return request.get(`commodity/${id}/getCommodityById`)
}

//通过Id修改商品
export function updataCommodityById(id, data) {
    return request.post(`commodity/${id}/updataCommodityById`, data)
}

//通过Id修改规格
export function updatespecifications(id, data) {
    return request.put(`specifications/${id}/updatespecifications`, data)
}

//通过Id冻结规格
export function frozenspecifications(id, data) {
    return request.put(`specifications/${id}/frozenspecifications`, data)
}

//通过Id冻结图片
export function frozenimgs(id) {
    return request.put(`fileuploadprocessing/${id}/frozenimgs`)
}

//查询商品
export function commodityQuery(data) {
    return request.post(`commodity/query`, data);
}