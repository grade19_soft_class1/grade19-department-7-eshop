import request from "../utils/request";

export function getShopTypeBySuperiorId(id) {
    return request.get(`ShopType/${id}`);
}

export function UpdataShopTypeIsActived(id){
    return request.put(`shoptype/${id}/enableshop`)
}