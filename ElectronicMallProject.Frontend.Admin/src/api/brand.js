import request from "../utils/request";

export function BrandGet() {
    return request.get(`brand/BrandGet`);
}

export function BrandQuery(data) {
    return request.post(`brand/queryBrand`, data);
}