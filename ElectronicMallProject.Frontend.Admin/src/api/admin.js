import request from "../utils/request";

export function getUsers() {
    return request.get(`users/get`);
}

export function editUser(id, data) {
    return request.put(`users/${id}/changeusers`, data)
}

//关闭用户
export function frozenUser(id, data) {
    return request.put(`users/${id}/frozenusers`, data)
}

//启用用户
export function enableUser(id, data) {
    return request.put(`users/${id}/enableusers`, data)
}
//分配用户角色
export function distributionrole(id, data) {
    return request.put(`users/${id}/distributionrole`, data);
}

//查询用户
export function queryUsers(data) {
    return request.post(`users/userQuery`, data);
}