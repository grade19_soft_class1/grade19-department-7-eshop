import Router from 'vue-router';
import Vue from 'vue';
import routers from './route'

Vue.use(Router);

var route = new Router({
    mode: 'history',
    routes: routers
});



export default route;