const TokenKey = "Token";
const RefreshTokenKey = "RefreshToken";

export function getToken() {
  return localStorage.getItem(TokenKey);
}
export function getRefreshToken() {
  return localStorage.getItem(RefreshTokenKey);
}

export function setToken(token, refreshToken) {
  localStorage.setItem(TokenKey, token);
  localStorage.setItem(RefreshTokenKey, refreshToken);
}

export function removeToken() {
  localStorage.removeItem(TokenKey);
  localStorage.removeItem(RefreshTokenKey);
}
