import Vue from 'vue'
import 'normalize.css/normalize.css'
import App from './App.vue'
import router from './router/index'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/zh-CN'
import VCharts from 'v-charts'
import './icons' // icon
import store from './store'


Vue.use(ElementUI, { locale });
Vue.use(VCharts);
Vue.config.productionTip = false

new Vue({
    render: h => h(App),
    router,
    store
}).$mount('#app')